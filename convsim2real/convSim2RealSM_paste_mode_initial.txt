//-------------------------------------------------------------------------------------------------  
//---- Convert Postgres data of Gridlab-D into Oracle Real Meter format; and prepare ML -----------
//-------------------------------------------------------------------------------------------------  

import org.apache.spark._
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql._
import org.apache.spark.sql.SQLContext
import org.apache.spark.SparkContext._
import org.apache.spark.sql.Row
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Calendar
import java.sql.Timestamp
import java.math._

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer
import collection.JavaConversions._
import java.io.File
import com.typesafe.config.{Config, ConfigFactory, ConfigRenderOptions}

import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.stat.{MultivariateStatisticalSummary, Statistics}

import org.apache.commons.math3.fitting.PolynomialCurveFitter
import org.apache.commons.math3.fitting.WeightedObservedPoints


  val mdmHome = scala.util.Properties.envOrElse("MDM_HOME", "MDM/")
  val config = ConfigFactory.parseFile(new File(mdmHome + "src/main/resources/application.conf"))
  val numProcesses = config.getInt("mdms.numProcesses")
  val runmode = config.getInt("mdms.runmode")

  //val srcurl = config.getString("mdms.srcurl")
  val srcurl = "jdbc:postgresql://192.168.5.2:5433/sgdm?user=wendong&password=wendong"
  //val tgturl = "jdbc:postgresql://192.168.5.2:5433/sgdm?user=wendong&password=wendong"
  val tgturl2 = "jdbc:postgresql://192.168.5.2:5433/postgres?user=wendong&password=wendong"
  val tgturl = tgturl2

  val pgdti = config.getString("mdms.pgdti")

  val numClusters = config.getInt("mdms.numClusters")
  val numIters = config.getInt("mdms.numIters")
  val numRuns = config.getInt("mdms.numRuns")
  val numDays = 3258

  val numLoadtypes = config.getInt("mdms.numLoadtypes") // Industrial, Commercial, Residential
  val numSeasons   = config.getInt("mdms.numSeasons")   // Spring, Summer, Fall, Winter
  val numDaytypes  = config.getInt("mdms.numDaytypes")  // Weekday, Weekend, Holiday
  val minNumPoints = config.getInt("mdms.minNumPoints")
 
  // Number of Buckets to bin a range of active power data points
  val numBucketAP  = config.getInt("mdms.numBucketAP")
  val numBucketV   = config.getInt("mdms.numBucketV")

  val volt_low  = config.getDouble("mdms.volt_low")
  val volt_high = config.getDouble("mdms.volt_high")
  val volt220_nominal = config.getDouble("mdms.volt220_nominal")
  val volt110_nominal = config.getDouble("mdms.volt110_nominal")
  
  val interactiveMeter = config.getString("mdms.interactive_meter")
  val meterIDs = config.getLongList("mdms.meterids.ids")

  val pgHourGroup = "data_quality.hourgroup"
  val pgPVHG = "data_quality.pvhg"
  val pgmrdsum = "data_quality.mrdsum"
  val tblMIDs = "data_quality.meterids"

 
  import sqlContext.implicits._
    
 // UDF applied to DataFrame columns
  val toIntg = udf((d: java.math.BigDecimal) => d.toString.toInt)
  val toIntg2 = udf((d: java.math.BigDecimal) => d.toString.replaceAll("""\.0+$""", "").toInt)
  val toLong = udf((d: java.math.BigDecimal) => d.toString.toLong)
  val toLong2 = udf((d: java.math.BigDecimal) => d.toString.replaceAll("""\.0+$""", "").toLong)
  val toDouble = udf((d: java.math.BigDecimal) => d.toString.toDouble)
  val toStr = udf((d: java.sql.Timestamp) => d.toString.replaceAll("""\.0$""", ""))
  val toTimestamp = udf((d: String) => Timestamp.valueOf(d))
  val toDecimal = udf((d: Double) => scala.math.BigDecimal(d.toString))
  
  val toDate = udf( (d: java.sql.Timestamp) => d.toString.replaceAll("""\ .*$""", "") )
  
  val toVolLow = udf((d: String) => "Voltage-Low")
  val toVolHigh = udf((d: String) => "Voltage-High")
  val toVolMissing = udf((d: String) => "Voltage-Missing")
  val toPowerLow = udf((d: String) => "Power-Low")
  val toPowerHigh = udf((d: String) => "Power-High")
  val toPowerMissing = udf((d: String) => "Power-Missing")

  def conv2SDTI(ts: Timestamp) = {
      val cal = Calendar.getInstance()
      cal.setTime(ts)
      val mon = cal.get(Calendar.MONTH)
      val day = cal.get(Calendar.DAY_OF_WEEK)
      var sdti = 0
      var season = 0
      var daytype = 0

      if (mon == 2 || mon == 3 || mon == 4) {                 // Spring
        if (day == 2 || day == 3 || day == 4 || day == 5 || day == 6) {// Weekday
          sdti = 1; season = 1; daytype = 1
        }
        else if (day == 1 || day == 7) { // Weekend
          sdti = 2; season = 1; daytype = 2;
        }
        else
          sdti = -1 // day not recognized
      }
      else if (mon == 5 || mon == 6 || mon == 7 || mon == 8) { // Summer
        if (day == 2 || day == 3 || day == 4 || day == 5 || day == 6) {// Weekday
          sdti = 4; season = 2; daytype = 1
        }
        else if (day == 1 || day == 7) { // Weekend
          sdti = 5; season = 2; daytype = 2;
        }
        else
          sdti = -2
      }
      else if (mon == 9 || mon == 10)  {                       // Fall
        if  (day == 2 || day == 3 || day == 4 || day == 5 || day == 6) {// Weekday
          sdti = 7; season = 3; daytype = 1
        }
        else if (day == 1 || day == 7) { // Weekend
          sdti = 8; season = 3; daytype = 2;
        }
        else
          sdti = -3
      }
      else if (mon == 11 || mon == 0 || mon == 1)  {                       // Winter
        if  (day == 2 || day == 3 || day == 4 || day == 5 || day == 6) { // Weekday
          sdti = 10; season = 4; daytype = 1
        }
        else if (day == 1 || day == 7) { // Weekend
          sdti = 11; season = 4; daytype = 2;
        }
        else
          sdti = -4
      }
      else {
        sdti = -9 // Month not recognized
      }
      (sdti, season, daytype)
  }

  // Create UDF using function conv2SDTI
  val  toSDTI = udf(conv2SDTI(_: Timestamp)._1)
  val  toSeason = udf(conv2SDTI(_: Timestamp)._2)
  val  toDaytype = udf(conv2SDTI(_: Timestamp)._3)
  
    val totalMetersPerStation = 1635
    val meterStartNumber = 14
    val meterEndNumber = meterStartNumber + totalMetersPerStation - 1
    
    // Load tables from database (here is PostgreSQL) into DataFrame
    //val brDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "basereading"))
    val brDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "basereading", "partitionColumn" -> "timeperiod", "lowerBound" -> "2", "upperBound" -> "35037", "numPartitions" -> "100"))
    val brS1DF = brDF.filter(s"meter >= $meterStartNumber and meter <= $meterEndNumber").cache 
    
    val idoDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "identifiedobject"))
    val meterDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "meter"))
    val meterS1DF = meterDF.filter(s"meterid >= $meterStartNumber and meterid <= $meterEndNumber").cache
    
    val rdtyDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "readingtype"))
    val mskDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "measurementkind"))
    val dtitv = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "datetimeinterval"))
    val phaseDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "phase")).cache 
    
    brS1DF.count
    meterS1DF.count
    phaseDF.count
    
    // identifiedobject join meter, using column alias to avoid name conflict in later join
    val idoM = idoDF.as('ido).join(meterS1DF.as('m), $"ido.identifiedobjectid" === $"m.meterid").select($"ido.identifiedobjectid" as 'idoid)

    // identifiedobject join meter join basereading
    val mrDF = idoM.as('idom).join(brS1DF.as('br), $"idom.idoid" === $"br.meter").select($"br.meter" as 'meterid, $"br.timeperiod", $"br.readingtype", $"br.value").cache 
    mrDF.count
    
    // then join readingtype
    val mrtDF = mrDF.as('mr).join(rdtyDF.as('rdt), $"mr.readingtype" === $"rdt.readingtypeid").select($"mr.meterid", $"mr.timeperiod", $"mr.readingtype", $"mr.value", $"rdt.tou", $"rdt.measurementkind" as 'mesk, $"rdt.phases")

    // then join measurementkind
    val mrtmDF = mrtDF.as('mrt).join(mskDF.as('msk), $"mrt.mesk" === $"msk.measurementkindid").select($"mrt.meterid", $"mrt.timeperiod" as 'timepd, $"mrt.readingtype" as 'rdty, $"mrt.value" as 'value, $"mrt.tou", $"mrt.phases", $"msk.measurementkindid" as 'meskid, $"msk.code" as 'code).orderBy("meterid").cache
    mrtmDF.count  // OK
    mrDF.unpersist
    
    // Then join phase  (no phase info? phase = null)
    //val mrtmphDF = mrtmDF.as('mrtm).join(phaseDF.as('phs), $"mrtm.phases" === $"phs.phasecode").select($"mrtm.meterid", $"mrtm.timepd", $"mrtm.rdty", $"mrtm.value" as 'value, $"mrtm.tou", $"mrtm.phases", //$"mrtm.meskid", $"mrtm.code", $"phs.code" as 'phasename).cache 
    //mrtmphDF.count
    //mrtmDF.unpersist
    
    val mrtmphDF = mrtmDF
    
    // then join DATETIMEINTERVAL
    // Don't cache yet; may have filter later
    val mrtmphdt = mrtmphDF.as('mrtmph).join(dtitv as('dtitv), $"mrtmph.timepd" === $"dtitv.datetimeintervalid").select("meterid", "timepd", "rdty", "value", "tou", "phases", "meskid", "code", "datetimeintervalid", "end").cache 
    mrtmphdt.count
    mrtmDF.unpersist
    
    // Meter reading data summary
    // need to workaround data type problem - convert meterid to double data type
    // also add columns "oldvalue" & "newvalue"
    val mrdsDF = mrtmphdt.withColumn("meterid", toDouble(mrtmphdt("meterid"))).withColumn("mtime", toTimestamp(toStr(mrtmphdt("end")))).select("meterid", "datetimeintervalid", "mtime", "value", "code", "phases").orderBy("meterid", "datetimeintervalid").cache 
    mrdsDF.count
    mrtmphdt.unpersist
  
