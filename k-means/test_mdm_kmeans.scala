import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql._
import org.apache.spark.sql.SQLContext
import org.apache.spark.SparkContext._
import org.apache.spark.sql.Row
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Calendar
import java.sql.Timestamp
import java.math._

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer
import collection.JavaConversions._
import java.io.File
import com.typesafe.config.{Config, ConfigFactory, ConfigRenderOptions}

import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.stat.{MultivariateStatisticalSummary, Statistics}

import org.apache.commons.math3.fitting.PolynomialCurveFitter
import org.apache.commons.math3.fitting.WeightedObservedPoints

  val mdmHome = scala.util.Properties.envOrElse("MDM_HOME", "MDM/")
  val config = ConfigFactory.parseFile(new File(mdmHome + "src/main/resources/application.conf"))
  val numProcesses = config.getInt("mdms.numProcesses")
//config.root().render(ConfigRenderOptions.concise())
  val runmode = config.getInt("mdms.runmode")
  val srcurl = "jdbc:postgresql://192.168.5.2:5433/sgdm?user=wendong&password=wendong"
  //val tgturl = "jdbc:postgresql://192.168.5.2:5433/sgdm_for_etl_large?user=wendong&password=wendong"

  val numClusters = config.getInt("mdms.numClusters")
  val numIters = config.getInt("mdms.numIters")
  val numRuns = config.getInt("mdms.numRuns")
  val numDays = 3258

  val numLoadtypes = config.getInt("mdms.numLoadtypes") // Industrial, Commercial, Residential
  val numSeasons   = config.getInt("mdms.numSeasons")   // Spring, Summer, Fall, Winter
  val numDaytypes  = config.getInt("mdms.numDaytypes")  // Weekday, Weekend, Holiday
  val minNumPoints = config.getInt("mdms.minNumPoints")
 
  // Number of Buckets to bin a range of active power data points
  val numBucketAP  = config.getInt("mdms.numBucketAP")
  val numBucketV   = config.getInt("mdms.numBucketV")

  val volt_low  = config.getDouble("mdms.volt_low")
  val volt_high = config.getDouble("mdms.volt_high")
  val volt220_nominal = config.getDouble("mdms.volt220_nominal")
  val volt110_nominal = config.getDouble("mdms.volt110_nominal")
  
  val interactiveMeter = config.getString("mdms.interactive_meter")
  val meterIDs = config.getLongList("mdms.meterids.ids")

  val pgHourGroup = "data_quality.hourgroup"
  val pgPVHG = "data_quality.pvhg"

import sqlContext.implicits._

  val voltLow1 = 10.0
  val voltLow2 = 88.0
  val voltLow3 = 132.0
  val voltLow4 = 188.0
  val voltHigh = 236.0
  val voltHigh2 = 253.0
  val voltDefault = 220.0

  // table to write
  val pgdqVolt = "data_quality.volt"
  val pgdqVl = "data_quality.voltagelow"
  val pgdqVh = "data_quality.voltagehigh"
  val pgdqVo = "data_quality.voltageout"
  val pgtestvop = "data_quality.voltageout_phc"
  val pgbasereading = "basereading"
  val pgenddevice = "enddevice"
  val pgido = "identifiedobject"
  val pgmeter = "meter"
  val pgpvcurve = "data_quality.pvcurve"
  val pgqvcurve = "data_quality.qvcurve"
  val tblMIDs = "data_quality.meterids"

  //Now reading tables 
  val pvsdDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "data_quality.pvsd", "partitionColumn" -> "id", "lowerBound" -> "14", "upperBound" -> "1648", "numPartitions" -> "100")).cache
  val qvsdDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "data_quality.qvsd", "partitionColumn" -> "id", "lowerBound" -> "14", "upperBound" -> "1648", "numPartitions" -> "100")).cache
  
 var urdataMap =  scala.collection.mutable.Map[(Long, Int, Int),Int]()
 var urdata = new ArrayBuffer[Row @unchecked]()

 // function to define PV features
 def get24HoursPVFAll(sc: SparkContext, sqlContext: SQLContext, pvsdDF: DataFrame, qvsdDF: DataFrame) = {
    import sqlContext.implicits._
    var pvsdBuckets = pvsdDF.na.drop()
                        .filter(s"volt_c <= $volt_high and volt_c >= $voltLow2")
                        .withColumn("bucket", pmod($"dti", lit(96)))
                        .select("id", "ts", "volt_c", "power", "dti", "sdti", "season", "daytype", "bucket")
    // P feature
    var featureP = pvsdBuckets.groupBy($"id", $"bucket", $"season", $"daytype").agg(avg("power")).sort("id", "bucket", "season", "daytype").cache()
    featureP.count //force to be cached
    // V feature
    var featureV = pvsdBuckets.groupBy($"id", $"bucket", $"season", $"daytype").agg(avg("volt_c")).sort("id", "bucket", "season", "daytype").cache()
    featureV.count //force to be cached
    val arrfp = featureP.collect
    val arrfv = featureV.collect
    //if (runmode == 1 || runmode == 4)
    //  featureP.select("ID").distinct.coalesce(numProcesses).write.mode("overwrite").jdbc(tgturl, tblMIDs, new java.util.Properties)
    (arrfp, arrfv)
  }

  def getFeatureVector(sc: SparkContext, arrfp: Array[Row], arrfv: Array[Row], id: Long, se: Int, dt: Int) = {
    // Initialize Vector
    var hr0vPV = Vectors.dense(0); var hr1vPV = Vectors.dense(0); var hr2vPV = Vectors.dense(0); var hr3vPV = Vectors.dense(0);
    var hr4vPV = Vectors.dense(0); var hr5vPV = Vectors.dense(0); var hr6vPV = Vectors.dense(0); var hr7vPV = Vectors.dense(0);
    var hr8vPV = Vectors.dense(0); var hr9vPV = Vectors.dense(0); var hr10vPV = Vectors.dense(0); var hr11vPV = Vectors.dense(0);
    var hr12vPV = Vectors.dense(0); var hr13vPV = Vectors.dense(0); var hr14vPV = Vectors.dense(0); var hr15vPV = Vectors.dense(0);
    var hr16vPV = Vectors.dense(0); var hr17vPV = Vectors.dense(0); var hr18vPV = Vectors.dense(0); var hr19vPV = Vectors.dense(0);
    var hr20vPV = Vectors.dense(0); var hr21vPV = Vectors.dense(0); var hr22vPV = Vectors.dense(0); var hr23vPV = Vectors.dense(0);
    // Retrieve readings from array of Row given id, se, dt
    var fpi = for {
      r <- arrfp
      if (r.getDecimal(0).longValue == id && r(2) == se && r(3) == dt )
    } yield {r.getDecimal(4).doubleValue}
  if (fpi.size >= 12) {
    var fvi = for {
      r <- arrfv
      if (r.getDecimal(0).longValue == id && r(2) == se && r(3) == dt )
    } yield {r.getDecimal(4).doubleValue}
    //var hrpvData = sc.emptyRDD[Vector]
    // Preparing feature vector of N features per hour for each hour (total 24 hours)
    // Generate dense Vector for each hour, containing feature vector of power features and voltage features   
    var volt_nominal = 220.0 
    if (fvi(1) < 154) 
      volt_nominal = 110.0    
    if (fpi.size == 96) {
      hr0vPV = Vectors.dense(Array(fpi(1), fvi(1)/volt_nominal))
      hr1vPV = Vectors.dense(Array(fpi(5), fvi(5)/volt_nominal))
      hr2vPV = Vectors.dense(Array(fpi(9), fvi(9)/volt_nominal))
      hr3vPV = Vectors.dense(Array(fpi(13), fvi(13)/volt_nominal))
      hr4vPV = Vectors.dense(Array(fpi(17), fvi(17)/volt_nominal))
      hr5vPV = Vectors.dense(Array(fpi(21), fvi(21)/volt_nominal))
      hr6vPV = Vectors.dense(Array(fpi(25), fvi(25)/volt_nominal))
      hr7vPV = Vectors.dense(Array(fpi(29), fvi(29)/volt_nominal))
      hr8vPV = Vectors.dense(Array(fpi(33), fvi(33)/volt_nominal))
      hr9vPV = Vectors.dense(Array(fpi(37), fvi(37)/volt_nominal))
      hr10vPV = Vectors.dense(Array(fpi(41), fvi(41)/volt_nominal))
      hr11vPV = Vectors.dense(Array(fpi(45), fvi(45)/volt_nominal))
      hr12vPV = Vectors.dense(Array(fpi(49), fvi(49)/volt_nominal))
      hr13vPV = Vectors.dense(Array(fpi(53), fvi(53)/volt_nominal))
      hr14vPV = Vectors.dense(Array(fpi(57), fvi(57)/volt_nominal))
      hr15vPV = Vectors.dense(Array(fpi(61), fvi(61)/volt_nominal))
      hr16vPV = Vectors.dense(Array(fpi(65), fvi(65)/volt_nominal))
      hr17vPV = Vectors.dense(Array(fpi(69), fvi(69)/volt_nominal))
      hr18vPV = Vectors.dense(Array(fpi(73), fvi(73)/volt_nominal))
      hr19vPV = Vectors.dense(Array(fpi(77), fvi(77)/volt_nominal))
      hr20vPV = Vectors.dense(Array(fpi(81), fvi(81)/volt_nominal))
      hr21vPV = Vectors.dense(Array(fpi(85), fvi(85)/volt_nominal))
      hr22vPV = Vectors.dense(Array(fpi(89), fvi(89)/volt_nominal))
      hr23vPV = Vectors.dense(Array(fpi(93), fvi(93)/volt_nominal))
    }
    else if (fpi.size == 48) {
      hr0vPV = Vectors.dense(Array(fpi(1), fvi(1)/volt_nominal))
      hr1vPV = Vectors.dense(Array(fpi(3), fvi(3)/volt_nominal))
      hr2vPV = Vectors.dense(Array(fpi(5), fvi(5)/volt_nominal))
      hr3vPV = Vectors.dense(Array(fpi(7), fvi(7)/volt_nominal))
      hr4vPV = Vectors.dense(Array(fpi(9), fvi(9)/volt_nominal))
      hr5vPV = Vectors.dense(Array(fpi(11), fvi(11)/volt_nominal))
      hr6vPV = Vectors.dense(Array(fpi(13), fvi(13)/volt_nominal))
      hr7vPV = Vectors.dense(Array(fpi(15), fvi(15)/volt_nominal))
      hr8vPV = Vectors.dense(Array(fpi(17), fvi(17)/volt_nominal))
      hr9vPV = Vectors.dense(Array(fpi(19), fvi(19)/volt_nominal))
      hr10vPV = Vectors.dense(Array(fpi(21), fvi(21)/volt_nominal))
      hr11vPV = Vectors.dense(Array(fpi(23), fvi(23)/volt_nominal))
      hr12vPV = Vectors.dense(Array(fpi(25), fvi(25)/volt_nominal))
      hr13vPV = Vectors.dense(Array(fpi(27), fvi(27)/volt_nominal))
      hr14vPV = Vectors.dense(Array(fpi(29), fvi(29)/volt_nominal))
      hr15vPV = Vectors.dense(Array(fpi(31), fvi(31)/volt_nominal))
      hr16vPV = Vectors.dense(Array(fpi(33), fvi(33)/volt_nominal))
      hr17vPV = Vectors.dense(Array(fpi(35), fvi(35)/volt_nominal))
      hr18vPV = Vectors.dense(Array(fpi(37), fvi(37)/volt_nominal))
      hr19vPV = Vectors.dense(Array(fpi(39), fvi(39)/volt_nominal))
      hr20vPV = Vectors.dense(Array(fpi(41), fvi(41)/volt_nominal))
      hr21vPV = Vectors.dense(Array(fpi(43), fvi(43)/volt_nominal))
      hr22vPV = Vectors.dense(Array(fpi(45), fvi(45)/volt_nominal))
      hr23vPV = Vectors.dense(Array(fpi(47), fvi(47)/volt_nominal))
     // log.info(s"Found datapoints 48 in : $id, $season, $daytype")
    }
    else if (fpi.size == 24) {
      hr0vPV = Vectors.dense(Array(fpi(0), fvi(0)/volt_nominal))
      hr1vPV = Vectors.dense(Array(fpi(1), fvi(1)/volt_nominal))
      hr2vPV = Vectors.dense(Array(fpi(2), fvi(2)/volt_nominal))
      hr3vPV = Vectors.dense(Array(fpi(3), fvi(3)/volt_nominal))
      hr4vPV = Vectors.dense(Array(fpi(4), fvi(4)/volt_nominal))
      hr5vPV = Vectors.dense(Array(fpi(5), fvi(5)/volt_nominal))
      hr6vPV = Vectors.dense(Array(fpi(6), fvi(6)/volt_nominal))
      hr7vPV = Vectors.dense(Array(fpi(7), fvi(7)/volt_nominal))
      hr8vPV = Vectors.dense(Array(fpi(8), fvi(8)/volt_nominal))
      hr9vPV = Vectors.dense(Array(fpi(9), fvi(9)/volt_nominal))
      hr10vPV = Vectors.dense(Array(fpi(10), fvi(10)/volt_nominal))
      hr11vPV = Vectors.dense(Array(fpi(11), fvi(11)/volt_nominal))
      hr12vPV = Vectors.dense(Array(fpi(12), fvi(12)/volt_nominal))
      hr13vPV = Vectors.dense(Array(fpi(13), fvi(13)/volt_nominal))
      hr14vPV = Vectors.dense(Array(fpi(14), fvi(14)/volt_nominal))
      hr15vPV = Vectors.dense(Array(fpi(15), fvi(15)/volt_nominal))
      hr16vPV = Vectors.dense(Array(fpi(16), fvi(16)/volt_nominal))
      hr17vPV = Vectors.dense(Array(fpi(17), fvi(17)/volt_nominal))
      hr18vPV = Vectors.dense(Array(fpi(18), fvi(18)/volt_nominal))
      hr19vPV = Vectors.dense(Array(fpi(19), fvi(19)/volt_nominal))
      hr20vPV = Vectors.dense(Array(fpi(20), fvi(20)/volt_nominal))
      hr21vPV = Vectors.dense(Array(fpi(21), fvi(21)/volt_nominal))
      hr22vPV = Vectors.dense(Array(fpi(22), fvi(22)/volt_nominal))
      hr23vPV = Vectors.dense(Array(fpi(23), fvi(23)/volt_nominal))
    }
    else { // irregular data
      urdata += Row(id, se, dt)
      //log.info(s"Found irregular data: $id, $se, $dt")
      println(s"Found irregular data: $id, $se, $dt")
    }
  }  
    if (fpi.size > 12) {
      // Create RDD of Vector of Bin frequency for both power and voltage data for training
      var hrpvData = sc.parallelize(Array(hr0vPV, hr1vPV, hr2vPV, hr3vPV, hr4vPV, hr5vPV, hr6vPV, hr7vPV, hr8vPV, hr9vPV, hr10vPV,
                         hr11vPV, hr12vPV, hr13vPV, hr14vPV, hr15vPV, hr16vPV, hr17vPV, hr18vPV, hr19vPV, hr20vPV,
                         hr21vPV, hr22vPV, hr23vPV))
      // Return RDD, and flag for non-empty
      (hrpvData, 1)
    } else {
      (sc.emptyRDD[Vector], 0)
    }
  }

  // call kMeans 
  def kmclustAlg(parsedData: RDD[Vector], numClusters: Integer, numIters: Integer, numRuns: Integer) = {
    // Run k-means clustering algorithm once
    KMeans.train(parsedData, numClusters, numIters, numRuns)
  }
  
  // Get arrays of PV feature Vector; Also populate tblMIDs
  val (arrfp, arrfv) = get24HoursPVFAll(sc, sqlContext, pvsdDF, qvsdDF)


    var clustersLSDOpt: Option[KMeansModel] = None
    var pvhgmRDDOpt: Option[RDD[(Int, Iterable[Long])]] = None
    var numHourGroup: Int = 6
 
    //Read the meter id list 
    val midDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "data_quality.meterids")) 

    //val meterids = midDF.select("ID").rdd.collect 
    val meterids = midDF.select("id").map(r => r.getDecimal(0).toString.replaceAll("""\.0+$""", "").toLong).collect
    val mids = meterids.slice(0,3)  //just pick up some meters for testing 

    //Initialization 
    var numHourGroup: Int = 6
    
    val s = System.nanoTime
    //testing purposes only to clear Array Buffer, map does not need to be cleared
    //arrHGRow.clear
    //hgMap.clear
    //Loop for each id, each season, each daytype; For large loops, it takes very long time!!
    var argsList = ListBuffer[(Long,Int,Int)]()
    for (i <- 0 until mids.size) {
      for (se <- 1 to numSeasons) {
        for (dt <- 1 to numDaytypes/2) {
          argsList += ((mids(i),se,dt))
        }
      }
    }
    /**
     * @params: a tuple of meterId, season, and datetime
     * @return: two lists, one to append to arrHGRow, the other to append to hgMap
     */
    def kmeansMap(args:(Long,Int,Int)) = {
          val idx = args._1
          val se = args._2
          val dt = args._3
          var arrHGRowAppend = new ArrayBuffer[Row]()
          var hgMapAppend = scala.collection.mutable.Map[(Long, Int, Int, Long),Int]()
          println(s"Begin getFeatureVector: $idx, $se, $dt")          
          var (hrpvData2, nonEmptyFlag) = getFeatureVector(sc, arrfp, arrfv, idx, se, dt)
          var hrpvDataOrg = hrpvData2.cache()
          //var hrpvData = hrpvDataOrg.coalesce(1)  // do we need to try using only 1 partition to reduce shuffle cost??
          var hrpvData = hrpvDataOrg          
          //var hrpvData3 = hrpvDataOrg.repartition(800)          
          var hrpvDataZI = hrpvData.zipWithIndex
           if (nonEmptyFlag == 1) {
            //Call kMeans 
            var clustersLSD = kmclustAlg(hrpvData, numHourGroup, 20, numRuns)
            var clusterPVPred = hrpvData.map(x => clustersLSD.predict(x))
            var clusterPVMap = hrpvDataZI.zip(clusterPVPred)
            var pvhgmRDD = clusterPVMap.map{r => (r._2, r._1._2)}.groupByKey         //clusterPVMap.coalesce(1) ??
            var arrHGinfo = pvhgmRDD.collect  // it's not large , so we can use collect()    //pvhgmRDD.coalesce(1) ??
            // Populate Hour Group data
            for(hg <- 0 until arrHGinfo.size) {
              var arrhi = arrHGinfo(hg)._2.toArray // hour index array
              for(m <- 0 until arrhi.size) {
                arrHGRowAppend += Row(idx, se, dt, hg, arrhi(m))
                hgMapAppend += (idx, se, dt, arrhi(m)) -> hg
              }
            }
          }
          else {
            //clustersLSDOpt = None
            //pvhgmRDDOpt = NhgDFone
          }
          hrpvDataOrg.unpersist()
          println(s"Finished getFeatureVector: $idx, $se, $dt")
          (arrHGRowAppend,hgMapAppend)
    }

    def kmeansReduce(a:(scala.collection.mutable.ArrayBuffer[org.apache.spark.sql.Row], scala.collection.mutable.Map[(Long, Int, Int, Long),Int]),b:(scala.collection.mutable.ArrayBuffer[org.apache.spark.sql.Row], scala.collection.mutable.Map[(Long, Int, Int, Long),Int]))={
        (a._1++b._1,a._2++b._2)
    }

    val(arrHGRow,hgMap) = argsList.par.map(kmeansMap).reduce(kmeansReduce)
    val t =  System.nanoTime
    println(t-s)   

    val hgRowRDD = sc.parallelize(arrHGRow)

    val schemaHG = StructType(List(StructField("ID", LongType), StructField("Season", IntegerType), StructField("Daytype", IntegerType),
                                   StructField("hourgroup", IntegerType), StructField("hourindex", LongType)))

    val hgDF = sqlContext.createDataFrame(hgRowRDD, schemaHG).sort("ID", "Season", "Daytype", "hourgroup", "hourindex")

 //138491623854
   
