/** 
*  This is the ninth version of k-means that uses a self written version of k-means. 
*  As a result, it is the fastest run we have, but can only be run in local mode.
*  The program also keeps track of the centers and points for each clustering job and writes said information to two files specified by the streamingCentersTarget and streamingPointsTarget variables. This information should be copied to the HDFS later for streaming applications.
*  Note: These changes are not reflected in MLfuncs!
*/

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql._
import org.apache.spark.sql.SQLContext
import org.apache.spark.SparkContext._
import org.apache.spark.sql.Row
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Calendar
import java.sql.Timestamp
import java.math._
import scala.util.Random

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer
import collection.JavaConversions._
import java.io.File
import java.io._
        
import com.typesafe.config.{Config, ConfigFactory, ConfigRenderOptions}

import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.stat.{MultivariateStatisticalSummary, Statistics}

import org.apache.commons.math3.fitting.PolynomialCurveFitter
import org.apache.commons.math3.fitting.WeightedObservedPoints

  val mdmHome = scala.util.Properties.envOrElse("MDM_HOME", "MDM/")
  val config = ConfigFactory.parseFile(new File(mdmHome + "src/main/resources/application.conf"))
  val numProcesses = config.getInt("mdms.numProcesses")
//config.root().render(ConfigRenderOptions.concise())
  val runmode = config.getInt("mdms.runmode")
  val srcurl = "jdbc:postgresql://192.168.5.2:5433/sgdm?user=wendong&password=wendong"
  //val srcurl = "jdbc:postgresql://192.168.5.2:5433/postgres?user=postgres&password=root"
  //val tgturl = "jdbc:postgresql://192.168.5.2:5433/sgdm_for_etl_large?user=wendong&password=wendong"

  val streamingCentersTarget = "/home/admin/apps/MyStreamingKmeans_22Jul2016/models/clusterCenters.txt"
  val streamingPointsTarget = "/home/admin/apps/MyStreamingKmeans_22Jul2016/models/points.txt"

  val centersWriter = new PrintWriter(new FileWriter(streamingCentersTarget,false)) // false to override files
  val pointsWriter = new PrintWriter(new FileWriter(streamingPointsTarget,false)) 

  // toString method for points
  def toStringPoints(hrpvData: (List[((Double, Double), Int)], Long, Int, Int)):String = {
    var res = ""
    val points = hrpvData._1
    for(p <- points) 
    {
       res += p._1._1.toString
       res += ","
       res += p._1._2.toString
       res += ","
       res += p._2.toString
       res += ", "
    }
    res += hrpvData._2.toString
    res += ","
    res += hrpvData._3.toString
    res += ","
    res += hrpvData._4.toString
    res += "\n"
    res
  }

  // toString method for means
  def toStringMeans(meansData: (List[(Double, Double)], Long, Int, Int)):String = {
    var res = ""
    val points = meansData._1
    for(p <- points) 
    {
       res += p._1.toString
       res += ","
       res += p._2.toString
       res += ","
    }
    res += meansData._2.toString
    res += ","
    res += meansData._3.toString
    res += ","
    res += meansData._4.toString
    res += "\n"
    res
  }


  val numClusters = config.getInt("mdms.numClusters")
  val numIters = config.getInt("mdms.numIters")
  val numRuns = config.getInt("mdms.numRuns")
  val numDays = 3258

  val numLoadtypes = config.getInt("mdms.numLoadtypes") // Industrial, Commercial, Residential
  val numSeasons   = config.getInt("mdms.numSeasons")   // Spring, Summer, Fall, Winter
  val numDaytypes  = config.getInt("mdms.numDaytypes")  // Weekday, Weekend, Holiday
  val minNumPoints = config.getInt("mdms.minNumPoints")
 
  // Number of Buckets to bin a range of active power data points
  val numBucketAP  = config.getInt("mdms.numBucketAP")
  val numBucketV   = config.getInt("mdms.numBucketV")

  val volt_low  = config.getDouble("mdms.volt_low")
  val volt_high = config.getDouble("mdms.volt_high")
  val volt220_nominal = config.getDouble("mdms.volt220_nominal")
  val volt110_nominal = config.getDouble("mdms.volt110_nominal")
  
  val interactiveMeter = config.getString("mdms.interactive_meter")
  val meterIDs = config.getLongList("mdms.meterids.ids")

  val pgHourGroup = "data_quality.hourgroup"
  val pgPVHG = "data_quality.pvhg"

import sqlContext.implicits._

  val voltLow1 = 10.0
  val voltLow2 = 88.0
  val voltLow3 = 132.0
  val voltLow4 = 188.0
  val voltHigh = 236.0
  val voltHigh2 = 253.0
  val voltDefault = 220.0

  // table to write
  val pgdqVolt = "data_quality.volt"
  val pgdqVl = "data_quality.voltagelow"
  val pgdqVh = "data_quality.voltagehigh"
  val pgdqVo = "data_quality.voltageout"
  val pgtestvop = "data_quality.voltageout_phc"
  val pgbasereading = "basereading"
  val pgenddevice = "enddevice"
  val pgido = "identifiedobject"
  val pgmeter = "meter"
  val pgpvcurve = "data_quality.pvcurve"
  val pgqvcurve = "data_quality.qvcurve"
  val tblMIDs = "data_quality.meterids"

  //Now reading tables 
  val pvsdDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "data_quality.pvsd", "partitionColumn" -> "id", "lowerBound" -> "14", "upperBound" -> "1648", "numPartitions" -> "100")).cache
  val qvsdDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "data_quality.qvsd", "partitionColumn" -> "id", "lowerBound" -> "14", "upperBound" -> "1648", "numPartitions" -> "100")).cache
  
 var urdataMap =  scala.collection.mutable.Map[(Long, Int, Int),Int]()
 var urdata = new ArrayBuffer[Row @unchecked]()

 // function to define PV features
 def get24HoursPVFAll(sc: SparkContext, sqlContext: SQLContext, pvsdDF: DataFrame, qvsdDF: DataFrame) = {
    import sqlContext.implicits._
    var pvsdBuckets = pvsdDF.na.drop()
                        .filter(s"volt_c <= $volt_high and volt_c >= $voltLow2")
                        .withColumn("bucket", pmod($"dti", lit(96)))
                        .select("id", "ts", "volt_c", "power", "dti", "sdti", "season", "daytype", "bucket")
    // P feature
    var featureP = pvsdBuckets.groupBy($"id", $"bucket", $"season", $"daytype").agg(avg("power")).sort("id", "bucket", "season", "daytype").cache()
    featureP.count //force to be cached
    // V feature
    var featureV = pvsdBuckets.groupBy($"id", $"bucket", $"season", $"daytype").agg(avg("volt_c")).sort("id", "bucket", "season", "daytype").cache()
    featureV.count //force to be cached
    val arrfp = featureP.collect
    val arrfv = featureV.collect
    //if (runmode == 1 || runmode == 4)
    //  featureP.select("ID").distinct.coalesce(numProcesses).write.mode("overwrite").jdbc(tgturl, tblMIDs, new java.util.Properties)
    (arrfp, arrfv)
  }


 //** START K-MEANS BLOCK **//
 //get the mean of a cluster of points
 def clusterMean(points: List[(Double,Double)]): (Double,Double) = {
    val cumulative = points.reduceLeft((a: (Double,Double), b: (Double,Double)) => (a._1 + b._1, a._2 + b._2))
    return (cumulative._1 / points.length, cumulative._2 / points.length)
  }

 //get the mean of all the clusters
 def mean(clusters: Map[Int, List[(Double,Double)]]): List[(Double,Double)] = {
    // find cluster means
    var means = List[(Double,Double)]()
    for (clusterIndex <- clusters.keys)
       means = means :+ clusterMean(clusters(clusterIndex))
    means
   }
 
 //helper function for clustering 
 def closest(p: (Double,Double), means: List[(Double,Double)]): Int = {
      val distances = means.map(center => ((p._1-center._1)*(p._1-center._1) + (p._2-center._2)*(p._2-center._2)))
      return distances.zipWithIndex.min._2
    }

 //group points into clusters based off a set of means
 def cluster(means: List[(Double,Double)], points: List[(Double,Double)]): Map[Int, List[(Double,Double)]] = {
    // assignment step
    val newClusters =
      points.groupBy(
        p => closest(p, means))
    newClusters
 }

 // high level function that clusters and re-evaluates the means for k-means
 def iterate(means: List[(Double,Double)], points: List[(Double,Double)]): List[(Double,Double)] = {
     var clusters = cluster(means,points)
     var newMeans = mean(clusters)
     newMeans
  }

  // collect clustering results
  def collect(means: List[(Double,Double)], data: List[((Double, Double),Int)]) = {
     val newClusters = data.map( p => (p._2, closest(p._1, means)))
     newClusters
  }

  //main k-means function, input takes in a list of pairs of points and indices, with hourindex as the Int parameter
  // the function preserves the pairing of each point to its hourindex
  def kmeans(data: List[((Double, Double),Int)]) = {
    var k = 6 // number of clusters
    var numIters = 20 //number of iterations to run
    var points = data.map(x=>x._1)
    var means = Random.shuffle(points).take(k) // random initialization of means
    
    for (i <- 0 to numIters) {
      means = iterate(means, points)
    }
    (collect(means,data),means)
  }

  //** END K-MEANS BLOCK **//

  var hgMap = scala.collection.mutable.Map[(Long, Int, Int, Long),Int]()
  // Get arrays of PV feature Vector; Also populate tblMIDs
  val (arrfp, arrfv) = get24HoursPVFAll(sc, sqlContext, pvsdDF, qvsdDF)

    //var hgMap = scala.collection.mutable.Map[(Long, Int, Int, Long),Int]()
    var arrHrPVRDD = new ArrayBuffer[RDD[(Vector, Long)]]()
    var arrKMMOpt  = new ArrayBuffer[Option[KMeansModel]]()
    var arrHGOpt = new ArrayBuffer[Option[RDD[(Int, Iterable[Long])]]]()
    val arrHGRow = new ArrayBuffer[Row]()

    var clustersLSDOpt: Option[KMeansModel] = None
    var pvhgmRDDOpt: Option[RDD[(Int, Iterable[Long])]] = None
    var numHourGroup: Int = 6
 
    //Read the meter id list 
    val midDF = sqlContext.load("jdbc", Map("url" -> srcurl, "dbtable" -> "data_quality.meterids")) 

    //val meterids = midDF.select("ID").rdd.collect 
    val meterids = midDF.select("id").map(r => r.getDecimal(0).toString.replaceAll("""\.0+$""", "").toLong).collect
    var mids = meterids //pick up all meters

  //gets the feature vector for a given id, se, and dt
  //this function is used in a mapping call to convert fpi and fvi arrays into a format usable by k-means
  def getFeatureVector(fpi: Array[Double], fvi: Array[Double]) = {
    // Initialize 
    var hr0vPV = (0.0,0.0); var hr1vPV = (0.0,0.0); var hr2vPV = (0.0,0.0); var hr3vPV = (0.0,0.0);
    var hr4vPV = (0.0,0.0); var hr5vPV = (0.0,0.0); var hr6vPV = (0.0,0.0); var hr7vPV = (0.0,0.0);
    var hr8vPV = (0.0,0.0); var hr9vPV = (0.0,0.0); var hr10vPV = (0.0,0.0); var hr11vPV = (0.0,0.0);
    var hr12vPV = (0.0,0.0); var hr13vPV = (0.0,0.0); var hr14vPV = (0.0,0.0); var hr15vPV = (0.0,0.0);
    var hr16vPV = (0.0,0.0); var hr17vPV = (0.0,0.0); var hr18vPV = (0.0,0.0); var hr19vPV = (0.0,0.0);
    var hr20vPV = (0.0,0.0); var hr21vPV = (0.0,0.0); var hr22vPV = (0.0,0.0); var hr23vPV = (0.0,0.0);
  
  if (fpi.size >= 12) {
    
    //var hrpvData = sc.emptyRDD[Vector]
    // Preparing feature vector of N features per hour for each hour (total 24 hours)
    // Generate dense Vector for each hour, containing feature vector of power features and voltage features    
    var volt_nominal = 220.0 
    if (fvi(1) < 154) 
      volt_nominal = 110.0 
    var power_normalization = 1000*volt_nominal // turn to kiloWatts and then scale with volt_nominal again   
  if (fpi.size == 96) {
      hr0vPV = (fpi(1)/power_normalization, fvi(1)/volt_nominal)
      hr1vPV = (fpi(5)/power_normalization, fvi(5)/volt_nominal)
      hr2vPV = (fpi(9)/power_normalization, fvi(9)/volt_nominal)
      hr3vPV = (fpi(13)/power_normalization, fvi(13)/volt_nominal)
      hr4vPV = (fpi(17)/power_normalization, fvi(17)/volt_nominal)
      hr5vPV = (fpi(21)/power_normalization, fvi(21)/volt_nominal)
      hr6vPV = (fpi(25)/power_normalization, fvi(25)/volt_nominal)
      hr7vPV = (fpi(29)/power_normalization, fvi(29)/volt_nominal)
      hr8vPV = (fpi(33)/power_normalization, fvi(33)/volt_nominal)
      hr9vPV = (fpi(37)/power_normalization, fvi(37)/volt_nominal)
      hr10vPV = (fpi(41)/power_normalization, fvi(41)/volt_nominal)
      hr11vPV = (fpi(45)/power_normalization, fvi(45)/volt_nominal)
      hr12vPV = (fpi(49)/power_normalization, fvi(49)/volt_nominal)
      hr13vPV = (fpi(53)/power_normalization, fvi(53)/volt_nominal)
      hr14vPV = (fpi(57)/power_normalization, fvi(57)/volt_nominal)
      hr15vPV = (fpi(61)/power_normalization, fvi(61)/volt_nominal)
      hr16vPV = (fpi(65)/power_normalization, fvi(65)/volt_nominal)
      hr17vPV = (fpi(69)/power_normalization, fvi(69)/volt_nominal)
      hr18vPV = (fpi(73)/power_normalization, fvi(73)/volt_nominal)
      hr19vPV = (fpi(77)/power_normalization, fvi(77)/volt_nominal)
      hr20vPV = (fpi(81)/power_normalization, fvi(81)/volt_nominal)
      hr21vPV = (fpi(85)/power_normalization, fvi(85)/volt_nominal)
      hr22vPV = (fpi(89)/power_normalization, fvi(89)/volt_nominal)
      hr23vPV = (fpi(93)/power_normalization, fvi(93)/volt_nominal)
    }
    else if (fpi.size == 48) {
      hr0vPV = (fpi(1)/power_normalization, fvi(1)/volt_nominal)
      hr1vPV = (fpi(3)/power_normalization, fvi(3)/volt_nominal)
      hr2vPV = (fpi(5)/power_normalization, fvi(5)/volt_nominal)
      hr3vPV = (fpi(7)/power_normalization, fvi(7)/volt_nominal)
      hr4vPV = (fpi(9)/power_normalization, fvi(9)/volt_nominal)
      hr5vPV = (fpi(11)/power_normalization, fvi(11)/volt_nominal)
      hr6vPV = (fpi(13)/power_normalization, fvi(13)/volt_nominal)
      hr7vPV = (fpi(15)/power_normalization, fvi(15)/volt_nominal)
      hr8vPV = (fpi(17)/power_normalization, fvi(17)/volt_nominal)
      hr9vPV = (fpi(19)/power_normalization, fvi(19)/volt_nominal)
      hr10vPV = (fpi(21)/power_normalization, fvi(21)/volt_nominal)
      hr11vPV = (fpi(23)/power_normalization, fvi(23)/volt_nominal)
      hr12vPV = (fpi(25)/power_normalization, fvi(25)/volt_nominal)
      hr13vPV = (fpi(27)/power_normalization, fvi(27)/volt_nominal)
      hr14vPV = (fpi(29)/power_normalization, fvi(29)/volt_nominal)
      hr15vPV = (fpi(31)/power_normalization, fvi(31)/volt_nominal)
      hr16vPV = (fpi(33)/power_normalization, fvi(33)/volt_nominal)
      hr17vPV = (fpi(35)/power_normalization, fvi(35)/volt_nominal)
      hr18vPV = (fpi(37)/power_normalization, fvi(37)/volt_nominal)
      hr19vPV = (fpi(39)/power_normalization, fvi(39)/volt_nominal)
      hr20vPV = (fpi(41)/power_normalization, fvi(41)/volt_nominal)
      hr21vPV = (fpi(43)/power_normalization, fvi(43)/volt_nominal)
      hr22vPV = (fpi(45)/power_normalization, fvi(45)/volt_nominal)
      hr23vPV = (fpi(47)/power_normalization, fvi(47)/volt_nominal)
     // log.info(s"Found datapoints 48 in : $id, $season, $daytype")
    }
    else if (fpi.size == 24) {
      hr0vPV = (fpi(0)/power_normalization, fvi(0)/volt_nominal)
      hr1vPV = (fpi(1)/power_normalization, fvi(1)/volt_nominal)
      hr2vPV = (fpi(2)/power_normalization, fvi(2)/volt_nominal)
      hr3vPV = (fpi(3)/power_normalization, fvi(3)/volt_nominal)
      hr4vPV = (fpi(4)/power_normalization, fvi(4)/volt_nominal)
      hr5vPV = (fpi(5)/power_normalization, fvi(5)/volt_nominal)
      hr6vPV = (fpi(6)/power_normalization, fvi(6)/volt_nominal)
      hr7vPV = (fpi(7)/power_normalization, fvi(7)/volt_nominal)
      hr8vPV = (fpi(8)/power_normalization, fvi(8)/volt_nominal)
      hr9vPV = (fpi(9)/power_normalization, fvi(9)/volt_nominal)
      hr10vPV = (fpi(10)/power_normalization, fvi(10)/volt_nominal)
      hr11vPV = (fpi(11)/power_normalization, fvi(11)/volt_nominal)
      hr12vPV = (fpi(12)/power_normalization, fvi(12)/volt_nominal)
      hr13vPV = (fpi(13)/power_normalization, fvi(13)/volt_nominal)
      hr14vPV = (fpi(14)/power_normalization, fvi(14)/volt_nominal)
      hr15vPV = (fpi(15)/power_normalization, fvi(15)/volt_nominal)
      hr16vPV = (fpi(16)/power_normalization, fvi(16)/volt_nominal)
      hr17vPV = (fpi(17)/power_normalization, fvi(17)/volt_nominal)
      hr18vPV = (fpi(18)/power_normalization, fvi(18)/volt_nominal)
      hr19vPV = (fpi(19)/power_normalization, fvi(19)/volt_nominal)
      hr20vPV = (fpi(20)/power_normalization, fvi(20)/volt_nominal)
      hr21vPV = (fpi(21)/power_normalization, fvi(21)/volt_nominal)
      hr22vPV = (fpi(22)/power_normalization, fvi(22)/volt_nominal)
      hr23vPV = (fpi(23)/power_normalization, fvi(23)/volt_nominal)
    }
    else { // irregular data
      //log.info(s"Found irregular data: $id, $se, $dt")
      //println(s"Found irregular data")
    }
  }
    if (fpi.size > 12) {
      var hrpvData = List(hr0vPV, hr1vPV, hr2vPV, hr3vPV, hr4vPV, hr5vPV, hr6vPV, hr7vPV, hr8vPV, hr9vPV, hr10vPV,
                         hr11vPV, hr12vPV, hr13vPV, hr14vPV, hr15vPV, hr16vPV, hr17vPV, hr18vPV, hr19vPV, hr20vPV,
                         hr21vPV, hr22vPV, hr23vPV)
      (hrpvData.zipWithIndex, 1)
    } else {
      (List(((0.0,0.0),0)), 0) // garbage value so function return type is set
    }
  }

    // modified getFeatureVectors to pass arrfp and arrfv arrays only once
    // used so that mapping k-means is easier, also zips idx, se, and dt 
    // requires a set of meters to consider
    def getFeatureVectors(meters: Set[Long]) = {
      // get a map of all arrfp data
      var fpiAll = for {
         r <- arrfp
         } yield {((r.getDecimal(0).longValue, r.getInt(2), r.getInt(3)),r.getDecimal(4).doubleValue)}

      var fpiMap = fpiAll.groupBy(x=>x._1)
      
      // get a map of all arrfv data
      var fviAll = for {
         r <- arrfv
         } yield {((r.getDecimal(0).longValue, r.getInt(2), r.getInt(3)),r.getDecimal(4).doubleValue)}
 
      var fviMap = fviAll.groupBy(x=>x._1)

      // combine the two by key
      var fpifvi =  for {
         k <- fpiMap.keys }
         yield{(k,fpiMap(k), fviMap(k))}
      
      // and throw away the (idx,se,dt) pairs that were used for grouping
      var argsList = fpifvi.map(x=> (x._1, x._2.map(pair => pair._2), x._3.map(pair => pair._2)))
      
      //get feature vectors
      var resListWithFlag = argsList.map(x=>(getFeatureVector(x._2, x._3), x._1._1, x._1._2, x._1._3))

      //check and discard flag
      var res = for { 
         elem <- resListWithFlag if(elem._1._2 != 0)  // check the flag
      } yield {(elem._1._1, elem._2, elem._3, elem._4)}
      res
    }

    val s = System.nanoTime // start timing

    //get argument list and make a parallel list
    val hrpvParDataArray = getFeatureVectors(mids.toSet).toList.par
    //write points to output
    hrpvParDataArray.foreach(x=>pointsWriter.write(toStringPoints(x)))
    //map so that hrpvData is grouped with kmeans 
    val clustersLSDArrayPre = hrpvParDataArray.map(x=>(kmeans(x._1),x._2,x._3,x._4))
    val clustersLSDArray = clustersLSDArrayPre.map(x=>(x._1._1,x._2,x._3,x._4))
    val meansArray = clustersLSDArrayPre.map(x=>(x._1._2,x._2,x._3,x._4))
    //write means to output
    meansArray.foreach(x=>centersWriter.write(toStringMeans(x)))
    //next line folds in idx, se, and dt variables into data and flatmaps to get the correct number of rows
    var arrHGSeq = clustersLSDArray.map(x=>(x._1, List.fill(x._1.size)((x._2,x._3,x._4)))).map(x=>x._1 zip x._2).flatMap(x => x).map(x=>(x._2._1, x._2._2, x._2._3, x._1._2, x._1._1))

    //arrHGRow is turned into a list of Rows
    var arrHGRow = arrHGSeq.map(x=>Row(x._1,x._2,x._3,x._4,x._5))

    //forcible conversion of hourIndex into Long. Keep this on its own line or the table will not work!
    var arrHGRowNew = arrHGRow.map(r => Row(r.getLong(0), r.getInt(1), r.getInt(2), r.getInt(3), r.getInt(4).toLong)).seq

    var hgRowRDD = sc.parallelize(arrHGRowNew)

    var t =  System.nanoTime //end timing and check 
    println(t-s) 

    var schemaHG = StructType(List(StructField("ID", LongType), StructField("Season", IntegerType), StructField("Daytype", IntegerType),
                                   StructField("hourgroup", IntegerType), StructField("hourindex", LongType)))
    var hgDF = sqlContext.createDataFrame(hgRowRDD, schemaHG).sort("ID", "Season", "Daytype", "hourgroup", "hourindex")

    //close writers
    centersWriter.close()
    pointsWriter.close()
